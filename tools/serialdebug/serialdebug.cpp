﻿#include "serialdebug.h"

#include "ui_serialdebug.h"

#include <QDebug>
#include <QTimer>
// https://www.cnblogs.com/feiyangqingyun/p/3483764.html
// https://www.cnblogs.com/hanford/p/6048325.html
#include "gbk.h"
SerialDebug::SerialDebug(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::SerialDebug)
{
    ui->setupUi(this);

    //初始化串口设备
    serial = new QSerialPort(this);
    connect(serial, SIGNAL(readyRead()), this, SLOT(recvSlot()));

    //初始化定时器
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timerSlot()));

    //连接错误处理
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
        SLOT(errHandler(QSerialPort::SerialPortError)));

    //初始化界面
    initGui();

    recvNum = 0;
    sendNum = 0;
}

SerialDebug::~SerialDebug()
{
    serial->clear();
    serial->close();
    delete ui;
}

void SerialDebug::errHandler(QSerialPort::SerialPortError errNum)
{
    switch (errNum) {
    case QSerialPort::OpenError:
        ui->errLab->setText(
            "An error occurred while attempting to open an already opened device "
            "in this object");
        break;
    default:
        break;
    }
}

void SerialDebug::initGui()
{
    QStringList baudList; //波特率
    QStringList parityList; //校验位
    QStringList dataBitsList; //数据位
    QStringList stopBitsList; //停止位

    //根据连接串口显示可用串口号
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        ui->portBox->addItem(info.portName());
    }

    //波特率
    baudList << "1200"
             << "2400"
             << "4800"
             << "9600"
             << "19200"
             << "38400"
             << "57600"
             << "115200";
    ui->baudBox->addItems(baudList);
    ui->baudBox->setCurrentIndex(7); // QSerialPort::Baud1200 = 1200

    //奇偶校验
    parityList << "None"
               << "Even"
               << "Odd"
               << "Space"
               << "Mark";
    ui->parityBox->addItems(parityList);
    ui->parityBox->setCurrentIndex(QSerialPort::NoParity);

    //数据位
    dataBitsList << "5"
                 << "6"
                 << "7"
                 << "8";
    ui->dataBox->addItems(dataBitsList);
    ui->dataBox->setCurrentIndex(3); // QSerialPort::Data5 = 5

    //停止位
    stopBitsList << "1"
                 << "1.5"
                 << "2";
    ui->stopBox->addItems(stopBitsList);
    ui->stopBox->setCurrentIndex(0); // QSerialPort::OneAndHalfStop =  3
    ui->sendEdit->appendPlainText("https://gitee.com/coffeeLVeris/qt_open_icon");
}

void SerialDebug::on_openBtn_clicked()
{
    if (!serial->isOpen()) {
        serial->setPortName(ui->portBox->currentText());

        if (!serial->open(QIODevice::ReadWrite)) {
            ui->errLab->setText(a2w("打开串口失败"));
            qDebug() << serial->errorString();
            return;
        }

        setSerialAttr();
        ui->openBtn->setText(a2w("关闭串口"));
    } else {
        serial->clear();
        serial->close();
        ui->openBtn->setText(a2w("打开串口"));
    }
}

void SerialDebug::setSerialAttr()
{
    serial->setBaudRate(ui->baudBox->currentText().toInt());
    serial->setParity(QSerialPort::Parity(ui->parityBox->currentIndex()));
    serial->setDataBits(QSerialPort::DataBits(ui->dataBox->currentText().toInt()));

    QString stopStr = ui->stopBox->currentText();
    if (stopStr == "1") {
        serial->setStopBits(QSerialPort::OneStop);
    } else if (stopStr == "1.5") {
        serial->setStopBits(QSerialPort::OneAndHalfStop);
    } else {
        serial->setStopBits(QSerialPort::TwoStop);
    }

    serial->setRequestToSend(true); //设置 RTS 为高电平
    serial->setDataTerminalReady(true); //设置 DTR 为高电平
    serial->setFlowControl(QSerialPort::NoFlowControl);

    qDebug() << "setPortName=" << serial->portName();
    qDebug() << "setBaudRate=" << serial->baudRate();
    qDebug() << "setDataBits=" << serial->dataBits();
    qDebug() << "setParity=" << serial->parity();
    qDebug() << "setStopBits=" << serial->stopBits();
}

void SerialDebug::recvSlot()
{
    QString stamp;
    QString showModel = "ASCII";
    // https://blog.csdn.net/dengdew/article/details/79065608
    QByteArray buf;
    buf = serial->readAll();
    ui->recvNumLabel->setText("接收：" + QString::number(recvNum));
    if (ui->recvStopShowCheckBox->isChecked())
        return;
    if (ui->recvHexCheckBox->isChecked()) {
        buf = buf.toHex(' ');
        showModel = "HEX";
    }
    if (ui->showTimeCheckBox->isChecked()) {
        QDateTime dTime = QDateTime::currentDateTime();
        QString dateTime = dTime.toString("yyyy-MM-dd hh:mm:ss");
        stamp = m_stamp.arg("[ " + dateTime + " ]").arg("RECV").arg(showModel);
    } else
        stamp = m_stamp.arg(' ').arg("RECV").arg(showModel);
    ui->recvEdit->appendHtml(stamp);
    ui->recvEdit->appendPlainText(buf + '\n');
    recvNum += buf.length();
}

void SerialDebug::on_sendBtn_clicked()
{
    QString buf;
    buf = ui->sendEdit->toPlainText();
    buf.remove(QChar(' '), Qt::CaseInsensitive);
    sendSerialData(buf.toLocal8Bit().data(), buf.length());
}

void SerialDebug::sendSerialData(char *data, int length)
{
    // 回显
    if(true){
        QString stamp = m_stamp.arg(' ').arg("SEND").arg(ui->sendHexCheckBox->isChecked()?"HEX":"ASCII");
        ui->recvEdit->appendHtml(stamp);
        ui->recvEdit->appendPlainText(QString(data)+'\n');
    }
    serial->write(data, length);
    if (ui->sendAutoEmptyCheckBox->isChecked()) { //发送完成自动清空
        ui->sendEdit->clear();
    }
    if (ui->sendHexCheckBox->isChecked()) {
        length /= 2;
    }
    sendNum += length;
    ui->sendNumLabel->setText("发送：" + QString::number(sendNum));
}

void SerialDebug::on_sendEmptyBtn_clicked() { ui->sendEdit->clear(); }

void SerialDebug::on_recvEmptyBtn_clicked() { ui->recvEdit->clear(); }

void SerialDebug::timerSlot() { on_sendBtn_clicked(); }

void SerialDebug::on_cycleSendStopBtn_clicked() { timer->stop(); }

void SerialDebug::on_sendCycleCheckBox_clicked(bool checked)
{
    if (checked) { //数据流循环发送
        timer->start(ui->sendTimeLineEdit->text().toInt());
    } else {
        timer->stop();
    }
}

void SerialDebug::on_againScanBtn_clicked()
{
    ui->portBox->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        ui->portBox->addItem(info.portName());
    }
}

void SerialDebug::on_sendHexCheckBox_clicked(bool checked)
{
    QString buf = QString(ui->sendEdit->toPlainText());
    if (checked) {
        QByteArray tmp = buf.toLocal8Bit().toHex();
        int i = 2;
        while (i < buf.toLocal8Bit().length() * 3) {
            tmp.insert(i, QByteArray(" "));
            i += 3;
        }
        ui->sendEdit->setPlainText(tmp);
    } else {
        ui->sendEdit->setPlainText(QByteArray::fromHex(buf.toLocal8Bit()));
    }
}

void SerialDebug::on_resetNumBtn_clicked()
{
    ui->sendNumLabel->setText("发送：");
    ui->recvNumLabel->setText("接收：");
}
