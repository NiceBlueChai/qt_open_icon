﻿#ifndef NBCGBK_H
#define NBCGBK_H

/**
 * @author: NiceBlueChai
 * @email: bluechai@qq.com
 * @brief: 将gbk编码转为Unicode编码
 **/

// std::string (GBK) -> QString(Unicode)
#define a2w(str) NbcGbk::ToUnicode(str)
#include <QString>
#include <QTextCodec>
#include <string>
using std::string;

class NbcGbk {
public:
    inline static QString ToUnicode(const string& cstr)
    {
#ifdef _MSC_VER
        QTextCodec* pCodec = QTextCodec::codecForName("gb2312");
        if (!pCodec)
            return "";
        QString qstr = pCodec->toUnicode(cstr.c_str(), cstr.length());
        return qstr;
#else
        return QString(cstr.c_str());
#endif
    }
};

#endif // NBCGBK_H
